package fr.dawan.mvc1;

import javax.validation.Valid;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import fr.dawan.mvc1.formbeans.Contact;
import fr.dawan.mvc1.formbeans.Contact.Demande;

@Controller
public class ContactController {

	@GetMapping("/contact")
	public String contact(Model m) {
		m.addAttribute("contact-form", new Contact());
		m.addAttribute("typesList", Demande.values());
		return "contact-form";
	}

	@PostMapping("/send")
	public String send(Model m, @Valid @ModelAttribute("contact-form") Contact form, BindingResult result) {

		try {

			if (result.hasErrors()) {
				m.addAttribute("errors", result);
			} else {
				Email email = new SimpleEmail();
				email.setHostName("smtp.gmail.com");
				email.setSmtpPort(465);
				email.setAuthenticator(new DefaultAuthenticator("username", "password"));
				// remplacer username et password par des actifs pour recevoir le mail
				email.setSSLOnConnect(true);
				email.setFrom(form.getEmail());
				email.setSubject(form.getDemande().toString());
				email.setMsg(form.getMessage());
				email.addTo("yohann-m@hotmail.fr");
				email.send();
				m.addAttribute("msgRetour", "Message envoyé");
			}

		} catch (Exception e) {
			e.printStackTrace();
			m.addAttribute("msgRetour", "Erreur : " + e.getMessage());
			m.addAttribute("contact-form", form);
		}

		return "contact-form";
	}
}
