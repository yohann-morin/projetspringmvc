package fr.dawan.mvc1;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.dawan.mvc1.beans.User;
import fr.dawan.mvc1.dao.UserDao;
import fr.dawan.mvc1.formbeans.LoginForm;

@Controller
public class LoginController {

	@Autowired // on injectele bean créé au démarrage
	private UserDao userDao;

	@RequestMapping(value = "/authenticate", method = RequestMethod.GET) // ou @GetMapping(/authenticate")
	public String showLogin(Model model) {
//		L'objet loginForm sert à initialiser le formulaire
//		et à récupérer les données saisies une fois le formulaire validé
		LoginForm f = new LoginForm("admin@dawan.fr", "admin");
		model.addAttribute("login-form", f);
		return "login"; // WEB-INF/views/login.jsp
	}

	@PostMapping("/check-login")
	public String checkLogin(HttpServletRequest request, Model model, // pour pouvoir passer des params
			@Valid @ModelAttribute("login-form") LoginForm form, // formulaire envoyé
			BindingResult result) { // Si erreurs, result sera remplie
		String cible = "redirect:/admin/dashboard";

		if (result.hasErrors()) {
			model.addAttribute("errors", result);
			model.addAttribute("login-form", form);
			cible = "login";
		}

		User u = userDao.findByEmail(form.getEmail());
		if (u != null && u.getPassword().equals(form.getPassword())) {
//			ajout d'une variable en session
			request.getSession().setAttribute("email", form.getEmail());
			request.getSession().setAttribute("user_id", u.getId());
		} else {
			model.addAttribute("msg", "Erreur d'authentification !");
			model.addAttribute("login-form", form);
			cible = "login";
		}
		return cible;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

}
