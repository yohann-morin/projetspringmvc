package fr.dawan.mvc1.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fr.dawan.mvc1.beans.User;

public class UserDaoOLD {

	private static List<User> users;

	static {
		users = new ArrayList<>();
		users.add(new User((long) 1, "toto", "toto@dawan.fr", "toto"));
		users.add(new User((long) 2, "titi", "titi@dawan.fr", "titi"));
		users.add(new User((long) 3, "tata", "tata@dawan.fr", "tata"));
		users.add(new User((long) 4, "tutu", "tutu@dawan.fr", "tutu"));
		users.get(0).setAdmin(true);
	}

	public static List<User> findAll() {
		return users;
	}

	public static void insert(User u) {
		u.setId(new Long(users.size() + 1));
		users.add(u);
	}

	public static User findById(long id) {
		int pos = users.indexOf(new User(id, null, null, null));
		if (pos == -1)
			return null;
		return users.get(pos);
//		return users.stream().filter(u->u.getId()==id).findFirst().orElse(null);
	}

	public static void update(User u) {
		int pos = users.indexOf(u);
		users.set(pos, u);
	}

	public static void remove(long id) {
		int pos = users.indexOf(new User(id, null, null, null));
		if (pos != -1)
			users.remove(pos);
	}

	public static List<User> findByName(String rech) {
		return users.stream().filter(u -> u.getName().toLowerCase().contains(rech.toLowerCase()))
				.collect(Collectors.toList());
	}

}
