package fr.dawan.mvc1.formbeans;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class Contact {

	@NotEmpty
	private String name;

	@NotEmpty
	@Pattern(regexp = "\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b", message = "Email invalide")
	private String email;

	@NotEmpty
	private String message;

	@Enumerated(EnumType.STRING)
	private Demande demande;

	public enum Demande {
		RECLAMATION, QUESTION, AUTRE
	}

	public Contact() {
		super();
	}

	public Contact(@NotEmpty String name,
			@NotEmpty @Pattern(regexp = "\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b", message = "Email invalide") String email,
			@NotEmpty String message, Demande demande) {
		super();
		this.name = name;
		this.email = email;
		this.message = message;
		this.demande = demande;
	}

	public String contact() {

		return email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Demande getDemande() {
		return demande;
	}

	public void setDemande(Demande demande) {
		this.demande = demande;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
