package fr.dawan.mvc1.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.mvc1.beans.User;

/**
 * 
 * @author Admin stagiaire
 *
 */
public class Tools {

	/**
	 * Import d'un fichier csv
	 * 
	 * @param filePath
	 * @param myList
	 * @param columns
	 * @param separator
	 * @throws Exception
	 */
	public static <T> void toCsv(String filePath, List<T> myList, List<String> columns, String separator)
			throws Exception {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath))) {
			if (myList != null && myList.size() > 0) {
				Field[] tab = myList.get(0).getClass().getDeclaredFields();
				StringBuilder ligneEntete = new StringBuilder();
				for (Field f : tab) {
					f.setAccessible(true);
					if (columns.contains(f.getName()))
						ligneEntete.append(f.getName()).append(separator);
				}
				bw.write(ligneEntete.toString().substring(0, ligneEntete.length() - 1));
				bw.newLine();
				for (T obj : myList) {
					StringBuilder line = new StringBuilder();
					for (Field f : tab) {
						if (columns.contains(f.getName()))
							line.append(f.get(obj).toString()).append(separator);
					}
					bw.write(line.toString().substring(0, line.length() - 1));
					bw.newLine();
				}
			}
		}
	}

	public static List<User> importCsv(String filePath) throws Exception {
		List<User> users = new ArrayList<>();
		try (BufferedReader rd = new BufferedReader(new FileReader(filePath))) {
			rd.readLine(); // ligne d'entête qu'on enregistre pas
			String ligne = null;
			while ((ligne = rd.readLine()) != null) {
				if (!ligne.trim().isEmpty()) {
					// si ligne non vide
					String[] ul = ligne.split(";");
					// découper la ligne par rapport au ; (split)
					if (ul.length == 3) { // si 3 éléments dans le tableau
						try {
							User u = new User(); // créer un User et remplir : name,email, admin
							u.setName(ul[0]);
							u.setEmail(ul[1]);
							u.setAdmin(Boolean.parseBoolean(ul[2]));
							users.add(u); // ajouter l'utilisateur créé à la liste
						} catch (Exception ex) {
							// TODO logs
						}
					}
				}
			}
		}
		return users;
	}
}
