<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

	<h1>${title}</h1>
	
	<form:form method="post" action="admin/save-user" modelAttribute="u">
		
		<form:label path="name" >Nom :</form:label>
		<form:input path="name" />
		<br />
		
		<form:label path="email" >Email :</form:label>
		<form:input path="email" />
		<br />
		
		<c:choose>
			<c:when test="${isAdd}">
				<form:label path="password">Password :</form:label>
				<form:password path="password" showPassword="true"/>
				<br />
			</c:when>
			<c:otherwise>
				<form:hidden path="password" />
			</c:otherwise>
		</c:choose>
	
		<form:label path="admin" >Admin ?</form:label>
		<form:checkbox path="admin" />
		<br />
		
		<form:hidden path="id"/>
		<form:hidden path="version"/>
		
		<input type="submit" value="Enregistrer">
		
	</form:form>
	<div>${msg}</div>

