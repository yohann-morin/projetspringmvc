<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>


	<h1>Liste des utilisateurs</h1>
	<a href="admin/add-user">Ajouter un utilisateur</a>
	
	<fieldset>
		<legend>Recherche par nom :</legend>
		<form:form method="post" action="admin/search-user" modelAttribute="u">
			<form:input path="name"/>
			<input type="submit" value="Rechercher" />
		</form:form>
	</fieldset>
	<br />
	
	<c:choose>
		<c:when test="${resVide}">
			<p>Aucun résultat !<p>
		</c:when>
		<c:otherwise>
			<table border = "1">
				<tr>
					<th>Nom</th>
					<th>Email</th>
					<th>Admin?</th>
					<th>Actions</th>
				</tr>
				<c:forEach var="u" items="${users}">
					<tr>
						<td>${u.name }</td>
						<td>${u.email }</td>
						<td>
							<c:choose>
							<c:when test="${u.admin}">Oui</c:when>
							<c:otherwise>Non</c:otherwise>
							</c:choose>
						</td>
						<td>
							<a href="admin/users/update/${u.id}" title="Modifier">Modifier</a>
							<a href="admin/users/delete/${u.id}" title="Supprimer">Supprimer</a>
						</td>
					</tr>
				</c:forEach>
			</table>
			<div>
				 <c:choose>
                    <c:when test="${page>1}">
                         <a href="admin/users?page=${page-1}&max=${max}">Précédent</a>
                   	</c:when>
                    <c:otherwise><span>Précédent</span></c:otherwise>
                 </c:choose>
                 <span>${page}</span>
                 <c:choose>
                    <c:when test="${suivExist}">
                         <a href="admin/users?page=${page+1}&max=${max}">Suivant</a>
                    </c:when>
                    <c:otherwise>Suivant</c:otherwise>
                 </c:choose>
			</div>
		</c:otherwise>
	</c:choose>
	<br />
	
	<a href="admin/export-users" title="Exporter">Export CSV</a>
	
	<h4>Import CSV</h4>
	
	<form:form method="post" action="admin/upload-users" enctype="multipart/form-data">
		<input type="file" name="file" />
		<br />
		<input type="submit" value="Uploader" />	
	</form:form>
