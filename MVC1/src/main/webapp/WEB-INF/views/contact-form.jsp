<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


	<h1><spring:message code="contact-form.title"/></h1>
	
	<a href="${requestScope['javax.servlet.forward.request_uri']}?lang=fr">FRANCAIS</a>
    <a href="${requestScope['javax.servlet.forward.request_uri']}?lang=en">ENGLISH</a>
    <hr />
    
     <div style="color:red;">
                <spring:hasBindErrors name="contact-form">
                        <c:forEach var="err" items="${errors.allErrors}">
                                <c:out value="${err.field} :"/>
                                <c:out value="${err.defaultMessage} ;" />
                        </c:forEach>
                </spring:hasBindErrors>
        </div>
    
    <form:form method="post" action="send" modelAttribute="contact-form">
    	<form:label path="name">
			<spring:message code="contact-form.name" />
		</form:label>
		<form:input path="name" />
    	<br />
    	
    	<form:label path="email">
			<spring:message code="contact-form.email" />
		</form:label>
		<form:input path="email" />
		<br />
		
		<form:label path="demande">
			<spring:message code="contact-form.demande" />
		</form:label>
		<form:select path="demande"><form:options items="${typesList}" /></form:select>
		<br />
		
		<form:label path="message">
			<spring:message code="contact-form.message" />
		</form:label>
		<form:textarea path="message" cols="50" rows="4"/>
    	<br />
   
		<input type="submit" value="<spring:message code="contact-form.submit"/>">
		
    </form:form>
    <div>${msgRetour}</div>
