<%@ page contentType="text/html; charset=UTF-8" %>

<!-- intégration des balises jstl -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- bibliothèques de balises Spring -->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

	<h1>Authentification</h1>
	
	<a href="${requestScope['javax.servlet.forward.request_uri']}?lang=fr">FRANCAIS</a>
    <a href="${requestScope['javax.servlet.forward.request_uri']}?lang=en">ENGLISH</a>
	
	<div style="color:red;">
                <spring:hasBindErrors name="login-form">
                        <c:forEach var="err" items="${errors.allErrors}">
                                <c:out value="${err.field}" />
                                <c:out value="${err.defaultMessage}" />
                        </c:forEach>
                </spring:hasBindErrors>
        </div>
	
	<form:form method="post" action="check-login" modelAttribute="login-form">
		<form:label path="email">
			<spring:message code="login.lblEmail" />
		</form:label>
		<form:input path="email" />
		<br />
		<form:label path="password">
			<spring:message code="login.lblPassword"/>	
		</form:label>
		<form:password path="password"/>
		<br />
		<input type="submit" value="<spring:message code="login.lblSubmit"/>">
	</form:form>
	<div>${msg}</div>

